Categories:Office
License:GPLv2+
Web Site:http://www.mitzuli.com
Source Code:https://github.com/artetxem/mitzuli
Issue Tracker:https://github.com/artetxem/mitzuli/issues

Auto Name:Mitzuli
Summary:Offline Translator
Description:
Translator featuring a full offline mode, voice input (ASR), camera input
(OCR), voice output (TTS), and more!
.

Repo Type:git
Repo:https://github.com/artetxem/mitzuli

Build:1.0.2,10002
    disable=todo multiple apk
    commit=20f2c2f36e593f8c97fadecb3b95ed6cc0bad759
    subdir=app
    gradle=yes
    prebuild=echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java
    ndk=r10d

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.2
Current Version Code:10002

