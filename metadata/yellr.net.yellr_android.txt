Categories:Internet
License:GPLv2+
Web Site:https://github.com/hhroc/yellr-android/blob/HEAD/readme.md
Source Code:https://github.com/hhroc/yellr-android
Issue Tracker:https://github.com/hhroc/yellr-android/issues

Auto Name:Yellr
Summary:Provide free, open and anonymous feedback
Description:
Allows for anyone from the community to provide, free, open, and anonymous
feedback to published assignments, or whatever is on your mind!
.

Repo Type:git
Repo:https://github.com/hhroc/yellr-android

Build:0.1.5,1005
    commit=v0.1.5
    subdir=app
    gradle=yes

Maintainer Notes:
Switch to Tags or at least plain RepoManifest after the next version.
.

#Auto Update Mode:Version v%v
#Update Check Mode:Tags
Update Check Mode:RepoManifest/dev

